import './device.js'

let app = {

	device: {
		mobile: device.mobile(),
		tablet: device.tablet(),
		desktop: device.desktop(),
		meeGo: device.meego(),
		television: device.television(),
	},

	deviceModel: {
		iPad: device.ipad(),
		iPhone: device.iphone(),
		iPod: device.ipod(),
	},

	browser: {
		ie9: ie9(),
		ie10: ie10(),
		ie11: ie11(),
		edge: edge(),
		chrome: chrome(),
		opera: opera(),
		yandex: yandex(),
		safari: safari(),
		firefox: firefox(),
		ieMobile: ieMobile(),
	},

	os: {
		iOS: device.ios(),
		android: device.android(),
		blackBerry: device.blackberry(),
		wpOS: windowsPhone(),
		firefoxOS: device.fxos() && !windowsPhone() ? true : false,
		windows: windows(),
		macOS: macOS(),
		linux: linux(),
	},
	orientation: {
		landscape: device.landscape(),
		portrait: device.portrait(),
	}
}


function ie(){
	if (device.desktop() && document.all) {
		return true
	}else{
		return false
	}
}

function ie9(){
	if (device.desktop() && document.all && !window.atob) {
		return true
	}else{
		return false
	}
}

function ie10(){
	if (device.desktop() && Function('/*@cc_on return document.documentMode===10@*/')()) {
		return true
	}else{
		return false
	}
}

function ie11(){
	if (device.desktop() && !!window.MSInputMethodContext && !!document.documentMode) {
		return true
	}else{
		return false
	}
}

function edge(){
	if (device.desktop() && /Edge\/\d./i.test(navigator.userAgent)) {
		return true
	}else{
		return false
	}
}

function chrome(){
	if (/Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor) && !opera()) {
		return true
	}else{
		return false
	}
}

function opera(){
	if ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0) {
		return true
	}else{
		return false
	}
}


function yandex(){
	if (navigator.userAgent.toLowerCase().indexOf('yabrowser/') > -1) {
		return true
	}else{
		return false
	}
}

function safari(){
	if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1 && !windowsPhone()) {
		return true
	}else{
		return false
	}
}

function firefox(){
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
		return true
	}else{
		return false
	}
}

function ieMobile(){
	if (navigator.userAgent.match(/iemobile/i)) {
		return true
	}else{
		return false
	}
}



// ===========================================
//
//		OS
//
// ===========================================

function windows(){
	if (device.desktop() && navigator.appVersion.indexOf("Win") != -1) {
		return true
	}else{
		return false
	}
}

function linux(){
	if (device.desktop() && navigator.appVersion.indexOf("Linux") != -1) {
		return true
	}else{
		return false
	}
}

function macOS(){
	if (device.desktop() && navigator.appVersion.indexOf("Mac") != -1) {
		return true
	}else{
		return false
	}
}

function windowsPhone(){
	if (navigator.userAgent.match(/Windows Phone/i) || navigator.userAgent.match(/WPDesktop/i)) {
		return true
	}else{
		return false
	}
}

export {app};