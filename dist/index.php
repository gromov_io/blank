<!DOCTYPE html>
<html lang="en" views="index">
<head>
	<meta charset="UTF-8">
	<title>Gromov-templates</title>
</head>
<body class="content">
	<div class="intro">
		<div class="intro__title">Gromov-templates<span class="intro__sub-title"> Шаблон быстрого старта</span></div>
		
		<div class="intro__pre">Проверка PNG спрайтов</div>
		<div class="intro__helloween"></div>
		<div class="intro__sword"></div>

		<div class="intro__pre">Проверка SVG спрайтов</div>
		<svg class="intro__eye">
			<use xlink:href="#eye"></use>
		</svg>
	</div>

	<?php echo file_get_contents('img/svg-sprite.svg') ?>
	<script src="js/script.js"></script>
	<link rel="stylesheet" href="css/style.css">
</body>
</html>