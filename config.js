import webpackStream from 'webpack-stream';
const production = process.env.NODE_ENV === 'production';
const developers = !production ? true : false;


// Папка куда все соберается
const dist = './dist';


const config = {

	host: false,

	path: {
		dist: dist,

		img: './app/img',
		distImg: dist+'/img',

		js: './app/js',
		distJs: dist+'/js',

		css: './app/style',
		distCss: dist+'/css',

		fonts: './app/fonts',
		distFonts: dist+'/fonts',

		task: './task',
		core: './node_modules/gromov-core-templates',
		pngSprites: './app/sprite-png',
		svgSprites: './app/sprite-svg',

		cssTemplate: './node_modules/gromov-core-templates/png-sprite/template.mustache',
	},

	webpack: {
		entry: {
			script:'./app/js/script.js'
		},
		watch: developers ? './app/js/**/*.js' : null,
		devtool: developers ? 'cheap-inline-module-source-map' : null,
		module: {
			loaders: [{
				test: /\.js$/,
				loader: 'babel?presets[]=es2015'
			}],
			rules: [{
		        test: /\.vue$/,
		        loader: 'vue-loader',
		        options: {
		          postLoaders: {
		            html: 'babel-loader'
		          }
		        }
		    }]
		},
		output: {
			filename: 'script.js',
		},
		resolve: {
			alias: {
				'vue$': 'vue/dist/vue.esm.js'
			}
		},
		plugins: [
		  new webpackStream.webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery",
			"window.jQuery": "jquery"
		  })
		]
	},

	autoprefixer: {
		version: ['last 4 versions'],
	},

	styleMode: './app/style/style.styl',

	proxy: function() {
		if (this.host) {
			return false
		}else{
			return this.path.dist
		}
	},
};


if (production) {
	console.log();
	console.log('   ==>  Mod: production');
	console.log();
}

if (developers) {
	console.log();
	console.log('   ==>  Mod: developers');
	console.log();
}


export {production, developers, config};